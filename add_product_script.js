var filename = null,
    fs = require('fs'),
    csv = require('csv'),
    mongoose = require('mongoose')

mongoose.connect('mongodb://localhost/productDb');
require('./app/products.server.model');

var Product = mongoose.model('Product'),
    products = [];

function print_help() {
   console.log('Usage : node add_product_service.js -f <filename.csv>');
}

function err_and_exit(msg) {
    console.log('Error Encountered : ');
    console.log(msg);
    print_help();
    console.log('-----------------');
    console.log('Exiting');
    process.exit();
}

console.log('Starting fetching product list');
console.log('-----------------');

var ingestProductsFromCSV = function () {
    fs.readFile(filename, 'utf-8', readData);
};

var readData = function (error, data) {
    if (error) {
        log.info('Error occurred while reading from file');
        err_and_exit(error);
    }

    csv.parse(data, parseCSV);
};

var parseCSV = function (err, data) {
    if (err) {
        log.info('Error occurred while parsing the csv file');
        err_and_exit(err);
    }

    console.log('Total records in csv file: ' + data.length);
    for (var i = 1; i < data.length; i++) {

        if (data[i][0] === "") {
            console.log('Warning: Product name not provided. Skipping row : ' + (i + 1));
            continue;
        }

        if (data[i][1] === "") {
            console.log('Warning: Product Price not present. Skipping row : ' + (i + 1));
            continue;
        }

        var product_content = {
            'name': data[i][0],
            'price': data[i][1],
            'quantity': data[i][2]
        };

        products.push(product_content);
    }

    importToMongoDB();
};


var importToMongoDB = function () {
    console.log('Connecting to MongoDB');
    console.log('----------');

    console.log('Total records to save : ' + products.length);

    var duplicateRecords = 0;
    for (var i = 0; i < products.length; i++) {
        (function (i) {

            var product_content = {
                'name': products[i].name,
                'price': products[i].price,
                'quantity': products[i].quantity
            };

            var obj = new Product(product_content);

            obj.save(function (err) {
                if (err) {
                    err_and_exit(err);
                } else {
                    if (i === products.length-1) {

                        console.log("Completed saving products from CSV file!");
                        process.exit();
                    }
                }
            });
            
        })(i);
    }
};

if (process.argv.indexOf("-f") != -1) {
    filename = process.argv[process.argv.indexOf("-f") + 1]; //grab the next item

    console.log('Reading File ' + filename);
    console.log('--- --- ---');

    if (filename === undefined) {
        err_and_exit("No file passed for parameter '-f'");
    } else {
        ingestProductsFromCSV();
    }
} else {
    err_and_exit("No file argument '-f' was passed");
}
