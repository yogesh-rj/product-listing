	
	var express  = require('express');
    var app      = express();                               // create our app with express
    var mongoose = require('mongoose');                     // mongoose for mongodb
    var products = require('./app/product.server.controller');

    mongoose.connect('mongodb://localhost/productDb');

    app.use(express.static(__dirname + '/public'));

    app.route('/products/:page')
        .get(products.get_product_list);

    // listen (start app with node server.js) ======================================
    app.get('*', function(req, res) {
        res.sendFile('../public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });
    app.listen(8080);
    console.log("App listening on port 8080");