
var productList = angular.module('productList', ['zInfiniteScroll']);

angular.module('productList').controller('ProductListController', ['$scope', '$rootScope','ProductService',
	function($scope, $rootScope, ProductService) {
		var _this = this;

		_init = function(){
			_this.page = 1
			_this.getProductsToDisplay();
		};

		_this.getProductsToDisplay = function(){
			ProductService.getProductList(_this.page).then(function(successResponse){
				if(_this.page == 1){
					_this.products = successResponse.data;
				}else{
					_this.products.push(successResponse.data);
				}

			},function(errorResponse){
				alert(errorResponse.message);
			});
		};

		$scope.loadMore = function(){
			_this.page = _this.page + 1
			_this.getProductsToDisplay();
		}
		
		_init()
	}
])

angular.module('productList').service('ProductService', ['$http',
	function ($http) {
		var _this = this;
		_this.getProductList = function(page_no) {
			return $http.get('http://localhost:8080/products/'+page_no);
		};
	}
])