Requirements:
Node 5.x,
NPM 3.8.x

Steps to run this project

1.Clone this repo on your machine.

2.After cloning goto /product-listing

3.Run npm install

4.Run bower install

5.run node -f add_product_script.js to add product list into database

6.run node server.js to start node server

7.check output on http://localhost:8080/

Library Used:

ZInfiniteScroll(https://github.com/LightZam/zInfiniteScroll) to implement infinite scrolling.