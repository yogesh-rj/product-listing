'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var ProductSchema = new Schema({
	name: {
		type: String,
		default: ''
	},
	price: {
		type: Number
	},
	quantity:{
		type: Number
	}
});

mongoose.model('Product', ProductSchema);