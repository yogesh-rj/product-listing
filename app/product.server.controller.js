'use strict';

var mongoose = require('mongoose');
	require('./products.server.model');
var Product = mongoose.model('Product');


exports.get_product_list = function(req,res){
	var page = req.params.page;
	var per_page = 10;
	Product.find({}).skip((page-1)*per_page).limit(per_page).exec(function(error,products){
		if(error){
			return res.status(400).send({
				message: "Something went wrong!!"
			});
		}else{
			if(products.length > 0){
				res.json(products);
			}else{
				res.status(400).send({
					message: "No product found!!"
				});
			}
		}
	});
};